﻿using UnityEngine;

namespace Player
{
    public class Bullet : MonoBehaviour
    {
        private float _speed;
        private float _speedRotate;
        private Transform _goal;
        
        private Vector3 _lastDirectionToGoal;
        private float _timeRotate;
        private float _currentTimeRotate;

        private Coroutine _movementCoroutine;
        
        public void Init(float speed, float speedRotate, Transform goal)
        {
            _speed = speed;
            _speedRotate = speedRotate;
            _goal = goal;
        }

        private void Update()
        {
            Move();
            Rotate();
        }

        private void OnCollisionEnter(Collision collision)
        {
            var goal = collision.gameObject.GetComponentInParent<Goal.Goal>();
            if(goal == null || goal.transform != _goal) return;
            Destroy(gameObject);
        }

        private void Move()
        {
            transform.Translate(Vector3.forward * (Time.deltaTime * _speed));
        }

        private void Rotate()
        {
            var goalPosition = _goal.position;
            goalPosition.y = transform.position.y;
            
            transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(goalPosition - transform.position), Time.deltaTime * _speedRotate);
        }
    }
}