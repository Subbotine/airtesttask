﻿using UnityEngine;

namespace Player.Config
{
    [CreateAssetMenu(fileName = "Weapon Config", menuName = "Air/Weapon Config", order = 0)]
    public class WeaponConfig : ScriptableObject
    {
        [SerializeField] private float _reloadTime;
        [SerializeField] private float _speedMoveBullet;
        [SerializeField] private float _speedRotateBullet;
        [SerializeField] private Bullet _bulletPrefab;

        public float ReloadTime => _reloadTime;
        public float SpeedMoveBullet => _speedMoveBullet;
        public float SpeedRotateBullet => _speedRotateBullet;
        public Bullet BulletPrefab => _bulletPrefab;
    }
}