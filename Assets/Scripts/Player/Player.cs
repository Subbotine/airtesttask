﻿using Goal.Api;
using Player.Config;
using UnityEngine;
using Zenject;

namespace Player
{
    public class Player : MonoBehaviour
    {
        [Inject] private IGoalService _goalService;
        
        private const string PATH_WEAPON_CONFIG = "Weapon Config";

        [SerializeField] private Transform _shootPoint;
        
        private WeaponConfig _weaponConfig;

        private float _timeAfterShoot;
        
        private void Awake()
        {
            _weaponConfig = Resources.Load<WeaponConfig>(PATH_WEAPON_CONFIG);
        }

        private void Update()
        {
            _timeAfterShoot += Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space)) Shoot();
        }

        private void Shoot()
        {
            if(_timeAfterShoot < _weaponConfig.ReloadTime) return;
            _timeAfterShoot = 0;

            var goal = _goalService.GetNextGoal().transform;
            var directionPlayer = goal.position - transform.position;
            directionPlayer.y = 0;
            transform.forward = directionPlayer;
            var bullet = Instantiate(_weaponConfig.BulletPrefab, _shootPoint.position, _shootPoint.rotation);
            bullet.Init(_weaponConfig.SpeedMoveBullet, _weaponConfig.SpeedRotateBullet, goal);
        }
    }
}