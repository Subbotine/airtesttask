﻿namespace Goal.Api
{
    public interface IGoalService
    {
        Goal GetNextGoal();
    }
}