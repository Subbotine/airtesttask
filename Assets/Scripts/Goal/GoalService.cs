﻿using System.Collections;
using System.Collections.Generic;
using Goal.Api;
using Goal.Config;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Goal
{
    public class GoalService : MonoBehaviour, IGoalService
    {
        private const string PATH_SERVICE_CONFIG = "Goal Service Config";
        private const string PATH_GOALS_CONFIGS = "Goal";
        
        private GoalServiceConfig _serviceConfig;
        private GoalConfig[] _goalConfigs;
        private List<Goal> _goals;
        private int _currentGoalIndex = -1;

        private void Awake()
        {
            _serviceConfig = Resources.Load<GoalServiceConfig>($"{PATH_GOALS_CONFIGS}/{PATH_SERVICE_CONFIG}");
            _goalConfigs = Resources.LoadAll<GoalConfig>(PATH_GOALS_CONFIGS);
        }

        private IEnumerator Start()
        {
            yield return CreateGoals();
        }

        public Goal GetNextGoal()
        {
            _currentGoalIndex++;
            if (_currentGoalIndex >= _goals.Count) _currentGoalIndex = 0;
            return _goals[_currentGoalIndex];
        }

        private IEnumerator CreateGoals()
        {
            _goals = new List<Goal>();
            
            for (int i = 0; i < _serviceConfig.CountGoals; i++)
            {
                var goalConfig = _goalConfigs[Random.Range(0, _goalConfigs.Length)];
                var position = new Vector3(Random.Range(-_serviceConfig.RangeSpawnGoals, _serviceConfig.RangeSpawnGoals), 
                    0, Random.Range(-_serviceConfig.RangeSpawnGoals, _serviceConfig.RangeSpawnGoals));
                var goal = Instantiate(goalConfig.GoalPrefab, position, Quaternion.identity, transform);
                goal.Init(goalConfig.GoalSpeed, goalConfig.MoveRange);
                _goals.Add(goal);
                
                yield return null;
            }
        }
    }
}