﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace Goal
{
    public class Goal : MonoBehaviour
    {
        [SerializeField] private float _distanceToRecalculatePath = 0.05f;
        
        private Vector3 _startPosition;
        private Vector3 _startMovePosition;
        private Vector3 _targetMovePosition;

        private float _speed;
        private float _range;

        private float _currentTime;
        private float _timePath;
        
        public void Init(float speed, float range)
        {
            _speed = speed;
            _range = range;
        }

        private void Start()
        {
            _startPosition = transform.position;
            CalculatePath();
        }

        private void Update()
        {
            MoveToTarget();
        }

        private void CalculatePath()
        {
            _startMovePosition = transform.position;
            _targetMovePosition = new Vector3(_startPosition.x + Random.Range(-_range, _range), 0,
                _startPosition.y + Random.Range(-_range, _range));

            _timePath = CalculateTimePath();
            _currentTime = 0;
        }

        private void MoveToTarget()
        {
            if(_targetMovePosition.Equals(Vector3.zero)) return;

            _currentTime += Time.deltaTime;
            transform.position = Vector3.Lerp(_startMovePosition, _targetMovePosition, _currentTime / _timePath);
            if (Vector3.Distance(transform.position, _targetMovePosition) < _distanceToRecalculatePath) CalculatePath();
        }

        private float CalculateTimePath()
        {
            return Vector3.Distance(_startMovePosition, _targetMovePosition) / _speed;
        }
    }
}