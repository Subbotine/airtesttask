﻿using UnityEngine;

namespace Goal.Config
{
    [CreateAssetMenu(fileName = "Goal Service Config", menuName = "Air/Goal Service Config", order = 0)]
    public class GoalServiceConfig : ScriptableObject
    {
        [SerializeField] private int _countGoals;
        [SerializeField] private float _rangeSpawnGoals;

        public int CountGoals => _countGoals;
        public float RangeSpawnGoals => _rangeSpawnGoals;
    }
}