﻿using UnityEngine;

namespace Goal.Config
{
    [CreateAssetMenu(fileName = "Goal Config", menuName = "Air/Goal Config", order = 0)]
    public class GoalConfig : ScriptableObject
    {
        [SerializeField] private Goal _goalPrefab;
        [SerializeField] private float _goalSpeed;
        [SerializeField] private float _moveRange;

        public Goal GoalPrefab => _goalPrefab;
        public float GoalSpeed => _goalSpeed;
        public float MoveRange => _moveRange;
    }
}
