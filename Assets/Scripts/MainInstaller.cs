using Goal;
using Goal.Api;
using UnityEngine;
using Zenject;

public class MainInstaller : MonoInstaller
{
    [SerializeField] private GoalService _goalService;
    
    public override void InstallBindings()
    {
        Container.Bind<IGoalService>().To<GoalService>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
    }
}